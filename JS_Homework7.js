 const goodArray = ['We', 'have', 'just', 'some', 'moments', 'in', 'life'];

function createNewElement1(array){
    const ulElement = document.createElement('ul');
    document.body.appendChild(ulElement);
    array.map(function(elem) {
        const elemLi = document.createElement('li');
        elemLi.innerHTML = elem;
        ulElement.appendChild(elemLi);
    });
}

console.log(createNewElement1(goodArray));